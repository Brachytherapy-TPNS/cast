#include "CastBuild.h"

CastBuild::CastBuild()
{
	m_tubeFunction = vtkImplicitPolyDataDistance::New();
	m_clipper = vtkClipPolyData::New();
	m_skin = NULL;
	m_tubes = NULL;
	m_thickness = 3;
	m_radius = 50;
}

CastBuild::~CastBuild()
{
	m_tubeFunction->Delete();
	m_clipper->Delete();
}

void CastBuild::SetSkin(vtkPolyData* skin)
{
	m_skin = skin;
}

void CastBuild::SetTubes(vtkPolyData* tubes)
{
	m_tubes = tubes;
}

void CastBuild::SetThickness(double thickness)
{
	m_thickness = thickness;
}

void CastBuild::Update()
{
	vtkPolyData* castBase = this->ComputeCastBase();
	m_output = castBase;
}


#ifdef BOOST_BUILD
void CastBuild::_StartTimer()
{
	m_timer.restart();
}

void CastBuild::_ElapsedTime(string tag, string msg)
{
	double elapsedTime = m_timer.elapsed();
	char outMsg[100];
	sprintf(outMsg, "[%s] %.3f-%s ", tag.c_str(), elapsedTime, msg.c_str());
	cout << outMsg << endl;
	
}

#else 
void CastBuild::_StartTimer()
{
}

void CastBuild::_ElapsedTime(string tag, string msg)
{
}
#endif // BOOST_BUILD

// Thicken the m_skin according to the thickness
// The skin surface is offset according to its normal vector and then gap between
// the offset seurface and the old surface is drawn
void CastBuild::_ThickenSkin()
{

	vtkPolyData* outputPD = vtkPolyData::New();

	vtkFeatureEdges* boundaryEdgesFounder = vtkFeatureEdges::New();
	boundaryEdgesFounder->SetInputData(m_clipper->GetOutput());
	boundaryEdgesFounder->BoundaryEdgesOn();
	boundaryEdgesFounder->FeatureEdgesOff();
	boundaryEdgesFounder->NonManifoldEdgesOff();
	boundaryEdgesFounder->ManifoldEdgesOff();
	boundaryEdgesFounder->Update();

	// Obtained shifted boundary
	vtkPolyData* boundaryShifted = vtkPolyData::New();
	vtkPolyData* boundaryOri = vtkPolyData::New();
	boundaryOri = boundaryEdgesFounder->GetOutput();
	cout << "No. of Cell: " << boundaryOri->GetNumberOfCells() << endl;
	cout << "No. of pts: " << boundaryOri->GetNumberOfPoints() << endl;
	_RestructurePD(boundaryOri);

	vtkMath::MultiplyScalar(m_shiftNormal, m_thickness);
	vtkTransform* shiftTransform = vtkTransform::New();
	shiftTransform->Translate(m_shiftNormal);
	vtkTransformFilter* transformFilter = vtkTransformFilter::New();
	transformFilter->SetInputData(boundaryOri);
	transformFilter->SetTransform(shiftTransform);
	transformFilter->Update();

	boundaryShifted->DeepCopy(transformFilter->GetOutput());

	// Should seperate the loop lines first

	// This Stores the output
	vtkAppendPolyData* adder = vtkAppendPolyData::New();
	adder->AddInputData(boundaryOri);
	adder->AddInputData(boundaryShifted);
	adder->Update();



	// Surface closer
	vtkPolyData* surfaceCloseRibbon = vtkPolyData::New();
	surfaceCloseRibbon->SetPoints(adder->GetOutput()->GetPoints());
	vtkCellArray* ribbonCellArray = vtkCellArray::New();
	vtkIdList* cellIdList = vtkIdList::New();
	_GetCellList(boundaryOri, cellIdList);

	// Pair up line segments from edge of two surfacesthat should have 
	// a surface drawn between them
	int l_looper = 0;
	for (int i = 0; i < boundaryOri->GetNumberOfCells(); i++)
	{
		vtkLine* l_line1 = vtkLine::New();
		vtkLine* l_line2 = vtkLine::New();
		vtkIdList* orgIDs = vtkIdList::New();
		vtkIdList* shiftedIDs = vtkIdList::New();
		boundaryOri->GetCellPoints(cellIdList->GetId(i), orgIDs);
		int j = orgIDs->GetId(0);
		int k = orgIDs->GetId(1);


		l_line1->GetPointIds()->SetId(0, orgIDs->GetId(0));
		l_line1->GetPointIds()->SetId(1, orgIDs->GetId(1));
		l_line2->GetPointIds()->SetId(0, orgIDs->GetId(0) + boundaryOri->GetNumberOfPoints());
		l_line2->GetPointIds()->SetId(1, orgIDs->GetId(1) + boundaryOri->GetNumberOfPoints());

		ribbonCellArray->InsertNextCell(l_line1);
		ribbonCellArray->InsertNextCell(l_line2);

		l_line1->Delete();
		l_line2->Delete();
		orgIDs->Delete();
		shiftedIDs->Delete();
	}

	// prepair the shifted skin
	transformFilter->SetInputData(m_clipper->GetOutput());
	transformFilter->Update();
	vtkPolyData* shiftedSkin = vtkPolyData::New();
	shiftedSkin->DeepCopy(transformFilter->GetOutput());

	// close the gap using the filter
	surfaceCloseRibbon->SetLines(ribbonCellArray);
	vtkRuledSurfaceFilter* rsfilter = vtkRuledSurfaceFilter::New();
	rsfilter->SetInputData(surfaceCloseRibbon);
	rsfilter->SetResolution(3, 3);
	rsfilter->SetRuledModeToResample();
	rsfilter->Update();
	outputPD->DeepCopy(rsfilter->GetOutput());

	adder->RemoveAllInputs();
	adder->AddInputData(shiftedSkin);
	adder->AddInputData(m_clipper->GetOutput());
	adder->AddInputData(outputPD);
	adder->Update();

	outputPD = adder->GetOutput();

	// Clean potentially repeated points
	vtkCleanPolyData* cleaner = vtkCleanPolyData::New();
	cleaner->SetInputData(outputPD);
	cleaner->ConvertLinesToPointsOn();
	cleaner->ConvertPolysToLinesOn();
	cleaner->ConvertStripsToPolysOn();
	cleaner->Update();

	outputPD = cleaner->GetOutput();
	m_output->DeepCopy(outputPD);

	// Delete stuff.
	cleaner->Delete();
	rsfilter->Delete();
	ribbonCellArray->Delete();
	surfaceCloseRibbon->Delete();
	transformFilter->Delete();
}

// Get a list of the cell id
void CastBuild::_GetCellList(vtkPolyData *inPD, vtkIdList * outList)
{
	int numOfList = inPD->GetNumberOfCells();
	int l_looper = 0;
	while (outList->GetNumberOfIds() < numOfList) {
		if (inPD->GetCell(l_looper)->GetNumberOfPoints() > 0) {
			outList->InsertNextId(l_looper);
		}
		l_looper += 1;
	}
}

// Restructure lines only so that adjacent point has incremental vtkID
void CastBuild::_RestructurePD(vtkPolyData * inPD)
{
	vtkPolyData* tmpHolder = vtkPolyData::New();
	vtkPoints* tmpPts = vtkPoints::New();
	vtkCellArray* tmpCellArr = vtkCellArray::New();


	int l_nextCell;
	int l_looper = 0;
	int l_prevID = -1;
	tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));
	while (tmpPts->GetNumberOfPoints() < inPD->GetNumberOfPoints()) {
		vtkIdList* neighborPtID = vtkIdList::New();
		vtkIdList* neighborCellID = vtkIdList::New();

		inPD->GetPointCells(l_looper, neighborCellID);
		if (neighborCellID->GetId(0) != l_prevID) {
			l_nextCell = neighborCellID->GetId(0);
			l_prevID = l_nextCell;
		}
		else {
			l_nextCell = neighborCellID->GetId(1);
			l_prevID = l_nextCell;
		}
		
		inPD->GetCellPoints(l_nextCell, neighborPtID);
		if (neighborPtID->GetId(0) != l_looper) {
			l_looper = neighborPtID->GetId(0);
			tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));
		}
		else
		{
			tmpPts->InsertNextPoint(inPD->GetPoint(l_looper));
			l_looper = neighborPtID->GetId(1);
		}
		vtkLine* l_line = vtkLine::New();
		l_line->GetPointIds()->SetId(0, tmpPts->GetNumberOfPoints() - 2);
		l_line->GetPointIds()->SetId(1, tmpPts->GetNumberOfPoints() - 1);
		tmpCellArr->InsertNextCell(l_line);

		neighborPtID->Delete();
		neighborCellID->Delete();
		l_line->Delete();
	}
	vtkLine* tmpline = vtkLine::New();
	tmpline->GetPointIds()->SetId(0, tmpPts->GetNumberOfPoints() - 1);
	tmpline->GetPointIds()->SetId(1, 0);
	tmpCellArr->InsertNextCell(tmpline);
	tmpline->Delete();

	tmpHolder->SetPoints(tmpPts);
	tmpHolder->SetLines(tmpCellArr);
	inPD->DeepCopy(tmpHolder);
}




vtkPolyData* CastBuild::ComputeCastBase()
{

	
	_StartTimer();
	_ElapsedTime("DEBUG", "Start 1st updating cutter...");
	vtkTriangleFilter* tfilter = vtkTriangleFilter::New();
	tfilter->SetInputData(m_tubes);
	tfilter->PassLinesOff();
	tfilter->PassVertsOff();
	tfilter->Update();

	vtkIntersectionPolyDataFilter* intersecFilter = vtkIntersectionPolyDataFilter::New();
	intersecFilter->SetInputData(0, tfilter->GetOutput());
	intersecFilter->SetInputData(1, m_skin);
	intersecFilter->SetSplitFirstOutput(1);
	intersecFilter->Update();



	//m_clipper->SetClipFunction(m_tubeFunction);
	m_clipper->SetInputData(m_skin);
	m_clipper->InsideOutOn();
	//m_clipper->Update();
	_ElapsedTime("DEBUG", "Clipper 1st update done");
	
	//Cast center
	_ElapsedTime("DEBUG", "Center of mass...");
	vtkCenterOfMass* comFilter = vtkCenterOfMass::New();
	comFilter->SetInputData(intersecFilter->GetOutput());
	comFilter->SetUseScalarsAsWeights(false);
	comFilter->Update();
	_ElapsedTime("DEBUG", "Elapse done");

	//Clip skin as cast base
	vtkSphere* sphere = vtkSphere::New();
	sphere->SetCenter(comFilter->GetCenter());
	sphere->SetRadius(m_radius);

	_ElapsedTime("DEBUG", "Start 2nd update clipper...");
	m_clipper->SetClipFunction(sphere);
	m_clipper->Update();
	_ElapsedTime("DEBUG", "Clipper 2nd update done");



	//offset the clipped skin
	vtkPolyDataNormals* normalGenerator = vtkPolyDataNormals::New();
	normalGenerator->SetInputData(m_clipper->GetOutput());
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->ComputeCellNormalsOff();
	normalGenerator->Update();
	cout << "normalGenerator" << endl;
	normalGenerator->GetOutput()->Print(cout);
	cout << "tuple 0 = "<< normalGenerator->GetOutput()->GetPointData()->GetNormals()->GetTuple(0)[0] <<endl;
	
	// Find average normal vector
	double factor = 0;
	m_shiftNormal[0] = 0;
	m_shiftNormal[1] = 0;
	m_shiftNormal[2] = 0;
	_StartTimer();
	vtkDataArray* normals = normalGenerator->GetOutput()->GetPointData()->GetNormals();
	for (int i = 0; i < normals->GetNumberOfTuples(); i ++)
	{
		m_shiftNormal[0] += normals->GetTuple(i)[0];
		m_shiftNormal[1] += normals->GetTuple(i)[1];
		m_shiftNormal[2] += normals->GetTuple(i)[2];
	}
	factor = vtkMath::Norm(m_shiftNormal);
	for (int i = 0; i < 3; i++)
	{
		m_shiftNormal[i] /= factor;
	}
	cout << factor;
	_ElapsedTime("DEBUG", "Finish searching array.");
	cout << "Averaged normal: " << m_shiftNormal[0] << "," << m_shiftNormal[1] << "," << m_shiftNormal[2] << endl;
	cout << "Magnitude: " << vtkMath::Norm(m_shiftNormal) << endl;
	//vtkTransformFilter* offsetTransform = vtkTransformFilter::New();
	//offsetTransform->SetInput(geomFilter->GetOutput());

	vtkPolyData* test = vtkPolyData::New();
	_ThickenSkin();

	return normalGenerator->GetOutput();
}

vtkPolyData* CastBuild::GetOutput()
{
	return m_output;
}
