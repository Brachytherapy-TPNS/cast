#ifndef CASTBUILD_H
#define CASTBUILD_H

#include <vtkPolyData.h>
#include <vtkImplicitPolyDataDistance.h>
#include <vtkClipPolyData.h>
#include <vtkCenterOfMass.h>
#include <vtkSphere.h>
#include <vtkPolyDataNormals.h>
#include <vtkPoints.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkTransformFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkGeometryFilter.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkIntersectionPolyDataFilter.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkFeatureEdges.h>
#include <vtkRuledSurfaceFilter.h>
#include <vtkLine.h>
#include <vtkKdTree.h>
#include <vtkAppendPolyData.h>
#include <vtkTriangle.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>

#if VTK_MAJOR_VERSION < 6
#define SetInputData SetInput
#endif


#define BOOST_BUILD
#ifdef BOOST_BUILD
#include <boost/timer.hpp>
#endif

using namespace std;

class CastBuild
{
public:
	CastBuild();
	~CastBuild();

	void SetSkin(vtkPolyData* skin);
	void SetTubes(vtkPolyData* tubes);
	void SetThickness(double thickness);
	void SetCastRadius(double radius);
	void Update();
	vtkPolyData* GetOutput();

private:
#ifdef BOOST_BUILD
	boost::timer m_timer;
#endif
	void _StartTimer();
	void _ElapsedTime(string tag, string msg);
	void _ThickenSkin();
	void _GetCellList(vtkPolyData*, vtkIdList*);
	void _RestructurePD(vtkPolyData*);

	//Computation functions
	vtkPolyData* ComputeCastBase();

	//Parameter
	double m_shiftNormal[3];
	double m_thickness;
	double m_radius;

	//I/O
	vtkPolyData*	m_skin;
	vtkPolyData*	m_tubes;
	vtkPolyData*	m_output;

	//Filters
	vtkImplicitPolyDataDistance* m_tubeFunction;
	vtkClipPolyData* m_clipper;
	//vtkClipPolyData* m_clipper2;
};

#endif