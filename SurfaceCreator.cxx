#include "SurfaceCreator.h"

SurfaceCreator::SurfaceCreator() 
{
	m_resample			 = vtkImageResample::New();
	m_surface			 = vtkMarchingCubes::New();
	m_connectivityFilter = vtkPolyDataConnectivityFilter::New();
	m_smoother			 = vtkWindowedSincPolyDataFilter::New();

	//Default value
	m_input  = NULL;
	m_output = NULL;
	m_factor = 0.4;
	m_value	 = -140; 
}

SurfaceCreator::~SurfaceCreator(void)
{
	m_resample->Delete();
	m_surface->Delete();
	m_connectivityFilter->Delete();
	m_smoother->Delete();
}

void SurfaceCreator::SetResamplingFactor(double factor)
{
	m_factor = factor;
}

void SurfaceCreator::SetInput(vtkImageData* input)
{
	m_input = input;
}

void SurfaceCreator::SetValue(int value)
{
	m_value = value;
}

bool SurfaceCreator::Update()
{	
	if (m_input==NULL) return true;

	m_resample->SetAxisMagnificationFactor(0,m_factor);
	m_resample->SetAxisMagnificationFactor(1,m_factor);
	m_resample->SetAxisMagnificationFactor(2,m_factor);
	m_resample->SetInputData(m_input);
	m_resample->Update();

	m_surface->SetInputData(m_resample->GetOutput());
	m_surface->ComputeNormalsOff();
	m_surface->ComputeGradientsOff();
	m_surface->ComputeScalarsOff();
	m_surface->SetValue(0, m_value);
	m_surface->Update();

	m_connectivityFilter->SetInputData(m_surface->GetOutput());
	m_connectivityFilter->SetExtractionModeToLargestRegion();
	m_connectivityFilter->Update();

	m_smoother->SetInputData(m_connectivityFilter->GetOutput());
	m_smoother->SetNumberOfIterations(15);
	m_smoother->BoundarySmoothingOff();
	m_smoother->FeatureEdgeSmoothingOff();
	m_smoother->SetFeatureAngle(120.0);
	m_smoother->SetPassBand(.001);
	m_smoother->NonManifoldSmoothingOn();
	m_smoother->NormalizeCoordinatesOn();
	m_smoother->Update();

    m_output = m_smoother->GetOutput();

	if (m_output==NULL) return true;

	return false;
}

vtkPolyData* SurfaceCreator::GetOutput()
{
	return m_output;
}


  



  



  



  



  



  



