#ifndef SURFACECREATOR_H
#define SURFACECREATOR_H

#include <vtkImageData.h>
#include <vtkImageResample.h>
#include <vtkMarchingCubes.h>
#include <vtkPolyData.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkSmartPointer.h>
#include <vtkWindowedSincPolyDataFilter.h>

class SurfaceCreator {

public:
    SurfaceCreator();
	~SurfaceCreator();

	void SetResamplingFactor(double factor);
    void SetInput(vtkImageData*input);
    void SetValue(int value); 
    bool Update();
    vtkPolyData* GetOutput();

private:
	//Parameter
	double	m_factor; 
    int		m_value;

	//I/O
    vtkImageData*	m_input;
    vtkPolyData*	m_output;

	//Filter
	vtkImageResample*				m_resample;
	vtkMarchingCubes*				m_surface;
	vtkPolyDataConnectivityFilter*	m_connectivityFilter;
	vtkWindowedSincPolyDataFilter*	m_smoother;
};

#endif