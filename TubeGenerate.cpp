#include "TubeGenerate.h"

TubeGenerate::TubeGenerate()
{
	m_points = vtkPoints::New();
	m_points->InsertNextPoint(0, 0, 0);
	m_output = vtkPolyData::New();
	m_tubeFilter = vtkTubeFilter::New();
	m_appendFilter = vtkAppendPolyData::New();
	m_lineSource = vtkLineSource::New();
	m_direction[0] = 1;
	m_direction[1] = 0;
	m_direction[2] = 0;
	m_tubeLength = 145;
	m_radius = 0.5;
}

TubeGenerate::~TubeGenerate()
{
	m_tubeFilter->Delete();
	m_lineSource->Delete();
	m_points->Delete();
}

void TubeGenerate::SetPoints(vtkPoints* points)
{
	m_points = points;
}

void TubeGenerate::SetDirection(double dir[3])
{
	m_direction[0] = dir[0];
	m_direction[1] = dir[1];
	m_direction[2] = dir[2];
}

void TubeGenerate::SetTubeLength(double length)
{
	m_tubeLength = length;
}

void TubeGenerate::SetRadius(double radius)
{
	m_radius = radius;
}

void TubeGenerate::Update()
{
	vector<vtkPolyData*> tubePtr;
	for (int i = 0; i < m_points->GetNumberOfPoints(); i++)
	{
		vtkPolyData* tube = vtkPolyData::New();
		ComputeSingleTube(m_points->GetPoint(i), tube);
		cout << "tube points: " << tube->GetNumberOfPoints() << endl;
		tubePtr.push_back(tube);
	}
	AddTubeToOutput(tubePtr);
}

void TubeGenerate::ComputeSingleTube(double point[3], vtkPolyData* tube)
{
	// Normalize direction vector
	double dir[3];
	double norm = sqrt(pow(m_direction[0], 2) + pow(m_direction[1], 2) + pow(m_direction[2], 2));
	dir[0] = m_direction[0] / norm;
	dir[1] = m_direction[1] / norm;
	dir[2] = m_direction[2] / norm;
	
	// Compute coordinate of Point2
	double point2[3];
	point2[0] = point[0] - dir[0] * m_tubeLength;
	point2[1] = point[1] - dir[1] * m_tubeLength;
	point2[2] = point[2] - dir[2] * m_tubeLength;

	// Create a line
	vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
	lineSource->SetPoint1(point[0], point[1], point[2]);
	lineSource->SetPoint2(point2[0], point2[1], point2[2]);
	lineSource->Update();

	// Create a tube (cylinder) around the line
	m_tubeFilter->SetInputData(lineSource->GetOutput());
	m_tubeFilter->SetRadius(m_radius); //default is .5
	m_tubeFilter->SetNumberOfSides(50);
	m_tubeFilter->Update();

	cout << "tube points inside singletube: " << m_tubeFilter->GetOutput()->GetNumberOfPoints() << endl;

	tube->DeepCopy(m_tubeFilter->GetOutput());
}

void TubeGenerate::AddTubeToOutput(vector<vtkPolyData*> tube)
{
	m_appendFilter->RemoveAllInputs();
	m_appendFilter->AddInputData(m_output);
	for each (vtkPolyData* var in tube)
	{
		m_appendFilter->AddInputData(var);
	}
	m_appendFilter->Update();
	
	m_output->DeepCopy(m_appendFilter->GetOutput());
	cout << "output points: " << m_output->GetNumberOfPoints() << endl;
}

vtkPolyData* TubeGenerate::GetOutput()
{
	return m_output;
}
