#ifndef TUBEGENERATE_H
#define TUBEGENERATE_H

#include <vtkTubeFilter.h>
#include <vtkLineSource.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkAppendPolyData.h>

using namespace std;

class TubeGenerate
{
public:
	TubeGenerate();
	~TubeGenerate();

	void SetPoints(vtkPoints* points);
	void SetDirection(double dir[3]);
	void SetTubeLength(double length);
	void SetRadius(double radius);
	void Update();
	vtkPolyData* GetOutput();
	//vtkPolyData* GetNthTube();

private:
	//Computation functions
	void ComputeSingleTube(double point[3], vtkPolyData* tube);
	void AddTubeToOutput(vector<vtkPolyData*> tube);

	//Parameter
	double m_direction[3];
	double m_tubeLength;
	double m_radius;

	//I/O
	vtkPolyData*	m_output;
	vtkPoints*		m_points;

	//Filters
	vtkAppendPolyData* m_appendFilter;
	vtkTubeFilter* m_tubeFilter;
	vtkLineSource* m_lineSource;
};

#endif