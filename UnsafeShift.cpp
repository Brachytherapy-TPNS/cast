/* 
 * File:   UnsafeShift.h
 * Author: Zhang Teng, PhD Candidate, DIIR, CUHK
 * Mailbox: zhangteng630@gmail.com
 * Purpose: Shift a surface to construct shell, but maybe unsafe
 *
 * Created on Oct 26, 2015, 16:01 PM
 */

#include "UnsafeShift.h"


UnsafeShift::UnsafeShift(void)
{
	m_point0[0]=m_point0[1]=m_point0[2]=0.0;
	m_point1[0]=m_point1[1]=0.0;
	m_point1[2]=1.0;
	m_surface = vtkSmartPointer< vtkPolyData >::New();
	m_shiftedSurface = vtkSmartPointer< vtkPolyData >::New();
	m_shell = vtkSmartPointer< vtkPolyData >::New();
}


UnsafeShift::~UnsafeShift(void)
{
}


int UnsafeShift::CheckGestureLine()
{
	double centerlineMidpoint[3];
	centerlineMidpoint[0] = (m_centerline->GetPoints()->GetPoint((m_centerline->GetNumberOfPoints())/2))[0];
	centerlineMidpoint[1] = (m_centerline->GetPoints()->GetPoint((m_centerline->GetNumberOfPoints())/2))[1];
	centerlineMidpoint[2] = (m_centerline->GetPoints()->GetPoint((m_centerline->GetNumberOfPoints())/2))[2];

	//Find bounding data
	double bounds[6];
	m_surface->GetBounds(bounds);

	double xMin = bounds[0];
	double xMax = bounds[1];
	double yMin = bounds[2];
	double yMax = bounds[3];
	double zMin = bounds[4];
	double zMax = bounds[5];

	//Find Edge nCenter
	double edgeCenter[8][3];
	edgeCenter[0][0] = (xMin + xMax) / 2;
	edgeCenter[0][1] = yMin;
	edgeCenter[0][2] = zMin;
	edgeCenter[1][0] = xMax;
	edgeCenter[1][1] = yMin;
	edgeCenter[1][2] = (zMin + zMax) / 2;
	edgeCenter[2][0] = (xMin + xMax) / 2;
	edgeCenter[2][1] = yMin;
	edgeCenter[2][2] = zMin;
	edgeCenter[3][0] = xMin;
	edgeCenter[3][1] = yMin;
	edgeCenter[3][2] = (zMin + zMax) / 2 ;
	edgeCenter[4][0] = (xMin + xMax) / 2;
	edgeCenter[4][1] = yMax;
	edgeCenter[4][2] = zMin;
	edgeCenter[5][0] = xMax;
	edgeCenter[5][1] = yMax;
	edgeCenter[5][2] = (zMin + zMax) / 2;
	edgeCenter[6][0] = (xMin + xMax) / 2;
	edgeCenter[6][1] = yMax;
	edgeCenter[6][2] = zMax;
	edgeCenter[7][0] = xMin;
	edgeCenter[7][1] = yMax;
	edgeCenter[7][2] = (zMin + zMax) / 2;


	vtkOBBTree* obbtree = vtkOBBTree::New();
	obbtree->SetDataSet(m_surface);
	obbtree->BuildLocator();
	vtkPoints* intersectionPoints = vtkPoints::New();
	vtkIdList* intersectionCells = vtkIdList::New();
	
	int num = 0;
	for(num = 0; (intersectionCells->GetNumberOfIds() != 1) && num <8 ; num++)
	{
		obbtree->IntersectWithLine(centerlineMidpoint, edgeCenter[num], intersectionPoints, intersectionCells);
	}

	memcpy( m_internalPoint, centerlineMidpoint, 3*sizeof(double) );
	memcpy( m_externalPoint, edgeCenter[num-1], 3*sizeof(double) );

	//debug
	std::cout<<"m_internalPoint = "<<m_internalPoint<<endl;
	std::cout<<"m_externalPoint = "<<m_externalPoint<<endl;

	return 0;
}

int UnsafeShift::SetCenterline(vtkPolyData* centerline)
{
	m_centerline = centerline;
	return 0;

}

int UnsafeShift::SetSurface( vtkPolyData * surface )
{
	//check whether surface is NULL
	if( surface == NULL )
	{
		std::cerr << "**Error setting to-be-drilled surface as NULL pointer" << std::endl;
		return 1;
	}
	//check whether surface contains no points nor cells
	if( surface->GetNumberOfPoints() == 0 )
	{
		std::cerr << "**Error setting to-be-drilled surface with no points" << std::endl;
		return 1;
	}
	if( surface->GetNumberOfCells() == 0 )
	{
		std::cerr << "**Error setting to-be-drilled surface with no cells" << std::endl;
		return 1;
	}

	m_tempSurface = surface;
	return 0;
}

void UnsafeShift::BuildGraph( void )
{
	//free memory
	for( size_t i = 0; i < m_graph.size(); ++i )
	{
		delete m_graph[i];
	}
	m_graph.clear();
	//check surface information
	if( m_surface == NULL || m_surface->GetNumberOfCells() == 0 || m_surface->GetNumberOfPoints() == 0 )
		return;
	//construct graph nodes
	for( vtkIdType id = 0; id < m_surface->GetNumberOfPoints(); ++id )
	{
		Node * node = new Node;
		node->id = id;
		m_surface->GetPoint( id , node->point );
		m_graph.push_back( node );
	}
	//build edges and point-cells map
	for( vtkIdType id = 0; id < m_surface->GetNumberOfCells(); ++id )
	{
		vtkCell *cell = m_surface->GetCell(id);
		for( vtkIdType pid0 = 0; pid0 < cell->GetNumberOfPoints(); ++pid0 )
		{
			for( vtkIdType pid1 = 0; pid1 < cell->GetNumberOfPoints(); ++pid1 )
			{
				if( pid0==pid1 )
					continue;
				//std::cout << cell->GetPointId(pid0) << "," << cell->GetPointId(pid1) << std::endl;
				m_graph[cell->GetPointId(pid0)]->neighbors.push_back( m_graph[cell->GetPointId(pid1)] );
			}
		}
	}
	for( size_t i = 0; i < m_graph.size(); ++i )
	{
		std::sort( m_graph[i]->neighbors.begin(), m_graph[i]->neighbors.end() );
		m_graph[i]->neighbors.erase( std::unique( m_graph[i]->neighbors.begin(), m_graph[i]->neighbors.end() ), m_graph[i]->neighbors.end() );
	}
	//calculate polydata point normals
	vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
	//normalGenerator->SetInputData( m_surface );
	normalGenerator->SetInputData( m_surface );
	normalGenerator->ComputeCellNormalsOff();
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->Update();
	vtkFloatArray *normals = vtkFloatArray ::SafeDownCast( normalGenerator->GetOutput()->GetPointData()->GetNormals() );
	for( size_t i = 0; i < m_graph.size(); ++i )
	{
		//for( int j = 0; j < 3; ++j)
			//m_graph[i]->normal[j] = normals->GetComponent( i , j );
		normals->GetTuple( i, m_graph[i]->normal );
	}
	//find standalone edges
	std::map< std::pair< vtkIdType, vtkIdType >, std::vector< vtkIdType > > pointPairCellsMap;
	for( vtkIdType cid = 0; cid < m_surface->GetNumberOfCells(); ++cid )
	{
		vtkCell * cell = m_surface->GetCell( cid );
		for( vtkIdType pid0 = 0; pid0 < cell->GetNumberOfPoints(); ++pid0 )
		{
			for( vtkIdType pid1 = 0; pid1 < cell->GetNumberOfPoints(); ++pid1 )
			{
				if( pid0==pid1 )
					continue;
				std::pair< vtkIdType, vtkIdType > pp;
				vtkIdType p0 = cell->GetPointId( pid0 );
				vtkIdType p1 = cell->GetPointId( pid1 );
				if( p0 < p1 )
				{
					pp.first = p0;
					pp.second = p1;
				}
				else
				{
					pp.first = p1;
					pp.second = p0;
				}
				std::map< std::pair< vtkIdType, vtkIdType >, std::vector< vtkIdType > >::iterator iter = pointPairCellsMap.find( pp );
				if( iter == pointPairCellsMap.end() )
				{
					std::pair< std::pair< vtkIdType, vtkIdType >, std::vector< vtkIdType > > ppcs;
					ppcs.first = pp;
					std::vector< vtkIdType > cs;
					cs.push_back( cid );
					ppcs.second = cs;
					pointPairCellsMap.insert( ppcs );
				}
				else
				{
					(*iter).second.push_back( cid );
				}
			}
		}
	}
	m_standaloneEdges.clear();
	for( std::map< std::pair< vtkIdType, vtkIdType >, std::vector< vtkIdType > >::iterator iter = pointPairCellsMap.begin();
		iter != pointPairCellsMap.end(); ++iter )
	{
		std::sort( (*iter).second.begin(), (*iter).second.end() );
		(*iter).second.erase( std::unique( (*iter).second.begin(), (*iter).second.end() ), (*iter).second.end() );
		//std::cout << (*iter).second.size() << std::endl;
		if( (*iter).second.size() == 1 )
		{
			m_standaloneEdges.push_back( (*iter).first );
		}
	}
//	std:cout << m_standaloneEdges.size() << std::endl;
}

void UnsafeShift::SetGestureLine( double point0[3], double point1[3] )
{
	memcpy( m_point0, point0, 3*sizeof(double) );
	memcpy( m_point1, point1, 3*sizeof(double) );
}

int UnsafeShift::FindDirectionOfNormals()
{
	vtkSmartPointer< vtkOBBTree > obbtree = vtkSmartPointer< vtkOBBTree >::New();
	obbtree->SetDataSet( m_surface );
	obbtree->BuildLocator();
	vtkSmartPointer< vtkPoints > intersectionPoints = vtkSmartPointer< vtkPoints >::New();
	vtkSmartPointer< vtkIdList > intersectionCells = vtkSmartPointer< vtkIdList >::New();
	obbtree->IntersectWithLine( m_point0, m_point1, intersectionPoints, intersectionCells );

	//debug
	std::cerr << "**line (" << m_point0[0] << "," << m_point0[1] << "," << m_point0[2] << ")"
			<< "-(" << m_point1[0] << "," << m_point1[1] << "," << m_point1[2] << ") intersect input surface with "
			<< intersectionCells->GetNumberOfIds() << " Cells. "<<std::endl;

	if( intersectionCells->GetNumberOfIds() != 1 )
	{
		std::cerr << "**Error: line (" << m_point0[0] << "," << m_point0[1] << "," << m_point0[2] << ")"
			<< "-(" << m_point1[0] << "," << m_point1[1] << "," << m_point1[2] << ") intersect input surface with "
			<< intersectionCells->GetNumberOfIds() << " Cells. Please reset anothter line" << std::endl;
		return 1;
	}
	double direction[3];
	direction[0] = m_point1[0] - m_point0[0];
	direction[1] = m_point1[1] - m_point0[1];
	direction[2] = m_point1[2] - m_point0[2];
	std::vector< bool > assigned( m_graph.size(), false );
	vtkCell *cell = m_surface->GetCell( intersectionCells->GetId(0) );
	std::vector< Node * > seeds;
	for( int i = 0; i < cell->GetNumberOfPoints(); ++i )
	{
		//double normal[3] = m_graph
		double * normal = m_graph.at( cell->GetPointId( i ) )->normal;
		double dotproduct = normal[0]*direction[0] + normal[1]*direction[1] + normal[2]*direction[2];
		if( dotproduct < 0 )
		{
			normal[0] = -normal[0];
			normal[1] = -normal[1];
			normal[2] = -normal[2];
		}
		assigned.at( cell->GetPointId(i) ) = true;
		seeds.push_back( m_graph.at(cell->GetPointId(i)) );
	}
	while( !seeds.empty() )
	{
		std::vector< Node * > neighbors;
		for( int i = 0; i < seeds.size(); ++i )
		{
			Node * s = seeds[i];
			double * normalS = s->normal;
			for( int j = 0; j < s->neighbors.size(); ++j )
			{
				Node * n = s->neighbors[j];
				if( assigned.at( n->id ) )
					continue;
				assigned.at( n->id ) = true;
				double * normalN = n->normal;
				double dotproduct = normalS[0]*normalN[0] + normalS[1]*normalN[1] + normalS[2]*normalN[2];
				if( dotproduct < 0 )
				{
					normalN[0] = -normalN[0];
					normalN[1] = -normalN[1];
					normalN[2] = -normalN[2];
				}
				neighbors.push_back( n );
			}
		}
		seeds.swap( neighbors );
	}
	return 0;
}

int UnsafeShift::GenerateAnotherSurafce()
{
	m_shiftedSurface->DeepCopy( m_surface );
	for( size_t i = 0; i < m_graph.size(); ++i )
	{
		double L2 = m_graph[i]->normal[0]*m_graph[i]->normal[0] + m_graph[i]->normal[1]*m_graph[i]->normal[1] + m_graph[i]->normal[2]*m_graph[i]->normal[2];
		if( L2 <= 0.0 )
		{
			std::cerr << "**Error: finding a normal length 0.0 " << std::endl;
			return 1;
		}
		if( L2 != 1.0 )
		{
			L2 = std::sqrt( L2 );
			m_graph[i]->normal[0] /= L2;
			m_graph[i]->normal[1] /= L2;
			m_graph[i]->normal[2] /= L2;
		}
		double x = m_graph[i]->point[0] + m_thickness * m_graph[i]->normal[0];
		double y = m_graph[i]->point[1] + m_thickness * m_graph[i]->normal[1];
		double z = m_graph[i]->point[2] + m_thickness * m_graph[i]->normal[2];
		m_shiftedSurface->GetPoints()->SetPoint( i, x, y, z );
	}
	return 0;
}

vtkPolyData * UnsafeShift::GetShiftSurface( void )
{
	return m_shiftedSurface;
}

int UnsafeShift::LinkStandaloneEdges()
{
	  vtkSmartPointer<vtkPoints> splinepoints =
    vtkSmartPointer<vtkPoints>::New();

	if( m_surface->GetNumberOfPoints() != m_graph.size() || m_shiftedSurface->GetNumberOfPoints() != m_graph.size() )
	{
		std::cerr << "**Error: Points number has been changed!" << std::endl;
		return 1;
	}
	if( m_surface->GetNumberOfCells() != m_shiftedSurface->GetNumberOfCells() )
	{
		std::cerr << "**Error: Cells number has been changed!" << std::endl;
		return 1;
	}
	//insert points
	vtkSmartPointer< vtkPoints > points = vtkSmartPointer< vtkPoints >::New();
	for( vtkIdType pid  = 0; pid < m_surface->GetNumberOfPoints(); ++pid )
	{
		points->InsertNextPoint( m_surface->GetPoint( pid ) );
	}
	for( vtkIdType pid = 0; pid < m_shiftedSurface->GetNumberOfPoints(); ++pid )
	{
		points->InsertNextPoint( m_shiftedSurface->GetPoint( pid ) );
	}
	m_shell->SetPoints( points );
	//insert cells
	vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
	for( vtkIdType cid = 0; cid < m_surface->GetNumberOfCells(); ++cid )
	{
		vtkCell* cell = m_surface->GetCell( cid );
		cells->InsertNextCell( cell );
	}
	for( vtkIdType cid = 0; cid < m_shiftedSurface->GetNumberOfCells(); ++cid )
	{
		vtkCell* cell = m_surface->GetCell( cid );
		for( vtkIdType pid = 0; pid < cell->GetNumberOfPoints(); ++pid )
			cell->GetPointIds()->SetId( pid, cell->GetPointId(pid)+m_surface->GetNumberOfPoints() );
		cells->InsertNextCell( cell );
	}
	//connect standalone edges
	for( size_t i = 0; i < this->m_standaloneEdges.size(); ++i )
	{
		vtkIdType pid00 = m_standaloneEdges[i].first;
		vtkIdType pid01 = m_standaloneEdges[i].second;
	
		vtkIdType pid10 = pid00 + m_surface->GetNumberOfPoints();
		vtkIdType pid11 = pid01 + m_surface->GetNumberOfPoints();
		vtkSmartPointer< vtkTriangle > triangle0 = vtkSmartPointer< vtkTriangle >::New();
		triangle0->GetPointIds()->SetId( 0, pid00 );
		triangle0->GetPointIds()->SetId( 1, pid10 );
		triangle0->GetPointIds()->SetId( 2, pid11 );
		cells->InsertNextCell( triangle0 );
		vtkSmartPointer< vtkTriangle > triangle1 = vtkSmartPointer< vtkTriangle >::New();
		triangle1->GetPointIds()->SetId( 0, pid00 );
		triangle1->GetPointIds()->SetId( 1, pid01 );
		triangle1->GetPointIds()->SetId( 2, pid11 );
		cells->InsertNextCell( triangle1 );
	}
	m_shell->SetPolys( cells );
	return 0;
}

vtkPolyData * UnsafeShift::GetOutput( void )
{
	return m_shell;
}

void UnsafeShift::SetThickness(double thickness)
{
	m_thickness= thickness;
}

void UnsafeShift::Update()
{
	m_surface->DeepCopy( m_tempSurface );

	this->BuildGraph();
	this->CheckGestureLine();
	this->SetGestureLine(m_internalPoint, m_externalPoint);
	this->FindDirectionOfNormals();
	this->GenerateAnotherSurafce(); 
	this->GetShiftSurface();
	this->LinkStandaloneEdges();
}