/* 
 * File:   UnsafeShift.h
 * Author: Zhang Teng, PhD Candidate, DIIR, CUHK
 * Mailbox: zhangteng630@gmail.com
 * Purpose: Shift a surface to construct shell, but maybe unsafe
 * Licience: GNU3.0, http://www.google.com.hk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=7&ved=0CDUQFjAGahUKEwjzrI343rnHAhVSI44KHVPmD0Y&url=http%3A%2F%2Fwww.gnu.org%2Flicenses%2Fgpl-3.0.en.html&ei=L9_WVbPZAdLGuATTzL-wBA&usg=AFQjCNH7LpUmtrHljmxxj2DxbaZqewm5Jw&sig2=CppY1yeHHlM4azNE2VDl1A
 *
 * Created on Oct 26, 2015, 16:01 PM
 */

#ifndef __UnsafaceShift_h__
#define __UnsafaceShift_h__

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cfloat>
#include <map>

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkPolyDataNormals.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkOBBTree.h>
#include <vtkTriangle.h>

class UnsafeShift
{
public:
	struct Node{
		vtkIdType id;
		double point[3];
		std::vector< Node * > neighbors;
		double normal[3];
	};

public:
	UnsafeShift(void);
	~UnsafeShift(void);
	
	int SetCenterline(vtkPolyData* centerline);
	int SetSurface( vtkPolyData * surface );
	void SetThickness(double thickness);
	void SetGestureLine( double point0[3], double point1[3] );

	vtkPolyData* GetShiftSurface( void );
	vtkPolyData* GetOutput( void );

public:
	void BuildGraph();
	int FindDirectionOfNormals();
	int GenerateAnotherSurafce();
	int LinkStandaloneEdges();
	int CheckGestureLine();

	void Update();

	void finished();
	void process(int);

private:
	vtkSmartPointer< vtkPolyData > m_surface;
	vtkSmartPointer< vtkPolyData > m_shiftedSurface;
	vtkSmartPointer< vtkPolyData > m_shell;
	std::vector< Node * > m_graph;
	std::vector< std::pair< vtkIdType, vtkIdType > > m_standaloneEdges;
	double m_point0[3];
	double m_point1[3];

	double m_thickness;
	vtkPolyData* m_tempSurface;
	vtkPolyData* m_centerline;

	double m_internalPoint[3];
	double m_externalPoint[3];

	
};

#endif