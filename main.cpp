#include <itkNiftiImageIO.h>
#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageToVTKImageFilter.h>
#include "SurfaceCreator.h"
#include <vtkActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPolyDataWriter.h>
#include "TubeGenerate.h"
#include "vtkProperty.h"
#include "CastBuild.h"

using namespace std;
using namespace itk;

typedef Image<double, 3> ImageType;
typedef ImageFileReader<ImageType> ReaderType;
typedef ImageToVTKImageFilter<ImageType> ConnectorType;

int main()
{
	//load test data
	ReaderType::Pointer reader = ReaderType::New();
	reader->SetFileName("../data/ct.nii.gz");
	reader->Update();
	ImageType::Pointer image = reader->GetOutput();

	ConnectorType::Pointer connector = ConnectorType::New();
	connector->SetInput(image);
	connector->Update();
	cout << "Finish loading image" << endl;

	//create skin actor
	SurfaceCreator* surfaceCreator = new SurfaceCreator;
	surfaceCreator->SetInput(connector->GetOutput());
	surfaceCreator->Update();
	vtkPolyDataMapper* surfaceMapper = vtkPolyDataMapper::New();
	surfaceMapper->SetInputData(surfaceCreator->GetOutput());
	vtkActor* surfaceActor = vtkActor::New();
	surfaceActor->SetMapper(surfaceMapper);

	////save the surface
	//vtkPolyDataWriter* writer = vtkPolyDataWriter::New();
	//writer->SetInput(surfaceCreator->GetOutput());
	//writer->SetFileName("../data/skin.vtp");
	//writer->Write();

	// create tubes
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();
	points->InsertNextPoint(10.5 ,-50.4, 93.1);
	points->InsertNextPoint(9, -48, 90);
	points->InsertNextPoint(11.5, -52, 95);
	double dir[3];
	dir[0] = 10.5-(-19);
	dir[1] = -50.4 - (-44);
	dir[2] = 93.1 - 164;
	TubeGenerate* tubeGenereate = new TubeGenerate;
	tubeGenereate->SetPoints(points);
	tubeGenereate->SetDirection(dir);
	tubeGenereate->Update();
	cout << "Finish generate tubes" << endl;

	// Create tube mapper and actor
	vtkSmartPointer<vtkPolyDataMapper> tubeMapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	tubeMapper->SetInputData(tubeGenereate->GetOutput());
	vtkSmartPointer<vtkActor> tubeActor =
		vtkSmartPointer<vtkActor>::New();
	tubeActor->GetProperty()->SetColor(1, 0.0, 0.0); // Give some color to the line
	tubeActor->SetMapper(tubeMapper);

	// Create cast
	CastBuild* castBuild = new CastBuild;
	castBuild->SetSkin(surfaceCreator->GetOutput());
	castBuild->SetTubes(tubeGenereate->GetOutput());
	castBuild->Update();
	cout << "Finish generate cast" << endl;

	// Create cast mapper and actor
	vtkSmartPointer<vtkPolyDataMapper> castMapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	castMapper->SetInputData(castBuild->GetOutput());
	vtkSmartPointer<vtkActor> castActor =
		vtkSmartPointer<vtkActor>::New();
	castActor->GetProperty()->SetColor(0.0, 0.0, 1.0); // Give some color to the line
	castActor->SetMapper(castMapper);

	//visualize
	vtkRenderer* ren = vtkRenderer::New();
	//ren->AddActor(surfaceActor);
	ren->AddActor(tubeActor);
	ren->AddActor(castActor);
	vtkRenderWindow* renWin = vtkRenderWindow::New();
	renWin->AddRenderer(ren);
	vtkRenderWindowInteractor* iren = vtkRenderWindowInteractor::New();
	iren->SetRenderWindow(renWin);
	vtkInteractorStyleTrackballCamera* style = vtkInteractorStyleTrackballCamera::New();
	renWin->GetInteractor()->SetInteractorStyle(style);
	renWin->Render();
	renWin->GetInteractor()->Initialize();
	renWin->GetInteractor()->Start();

}